//
//  ViewController.swift
//  Gestoure
//
//  Created by Fernando on 5/7/17.
//  Copyright © 2017 Fernando. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var tapsCount: UITapGestureRecognizer!
    @IBOutlet var touchcount: UITapGestureRecognizer!
    @IBOutlet weak var touchtextfile: UITextField!
    @IBOutlet weak var taptextfile: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tapsCount.numberOfTapsRequired = 0
    }
    
    @IBAction func tapOnview(_ sender: UITapGestureRecognizer) {
        
        self.view.backgroundColor = UIColor.cyan
        
    }
    @IBAction func txttap(_ sender: Any) {
        let taps:Int? = Int(taptextfile.text!)
        tapsCount.numberOfTapsRequired = taps!
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let n = touch!.tapCount
        let numtouch = Int(touchtextfile.text!)
        
        if n == numtouch{
        self.view.backgroundColor = UIColor(red: 0.48627450980392156, green: 0.070588235294117646, blue: 0.46274509803921571, alpha: 1)
        }
    }
    
}

